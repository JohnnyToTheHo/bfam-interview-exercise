package com.example.marketmaker;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class RequestProcessorFactory {
    public RequestProcessorFactory() {

    }

    public RequestProcessor createRequestProcessor(Message inMessage,ByteBuffer buffer, SocketChannel clientSocket,ReferencePriceSource referencePriceSource, QuoteCalculationEngine quoteCalculationEngine ) {
        return new RequestProcessor(inMessage,buffer,clientSocket, referencePriceSource, quoteCalculationEngine );
    }
}
