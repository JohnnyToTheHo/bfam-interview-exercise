package com.example.marketmaker.objects;


import com.example.marketmaker.QuoteCalculationEngine;

public class QuoteCalculationEngineObject implements QuoteCalculationEngine{

    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity){
        return referencePrice * quantity;
    }

}