package com.example.marketmaker.objects;

import java.util.HashMap;
import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;


public class ReferencePriceSourceObject implements ReferencePriceSource{

    @Override
    public void subscribe(ReferencePriceSourceListener listener){
        // todo to subscribe
    };


    @Override
    public double get(int securityId){
        // does not use any logic written above
        return securityId;
    };
}
