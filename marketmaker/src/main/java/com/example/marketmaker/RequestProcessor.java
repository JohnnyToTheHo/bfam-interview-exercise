package com.example.marketmaker;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.io.IOException;
import java.net.SocketException;

public class RequestProcessor implements Runnable {
    
    Message inMessage;
    SocketChannel clientSocket = null;
    QuoteCalculationEngine quoteCalculationEngine = null;
    ReferencePriceSource referencePriceSource = null;
    ByteBuffer buffer = null;

    public RequestProcessor(Message inMessage, ByteBuffer buffer, SocketChannel clientSocket,ReferencePriceSource referencePriceSource, QuoteCalculationEngine quoteCalculationEngine ){
        this.buffer = buffer;
        this.inMessage = inMessage;
        this.clientSocket= clientSocket;
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
    }

    @Override
    public void run(){
        try {
            if (!clientSocket.isOpen()){
                return;
            }
            if (inMessage.isExitMessage()){
                
                System.out.println("Connection with " + clientSocket.getRemoteAddress() + " has ended.");
                clientSocket.close();
                
            }
            else if(!inMessage.isValid()) {
                String invalid = "Invalid request\n";
                byte[] outMessage = new String(invalid).getBytes();
                buffer.clear();
                buffer.put(outMessage);
                buffer.flip();
                clientSocket.write(buffer);
                System.out.println("Response to " + inMessage.getMessage() + " : " + invalid);
            }
            else {
                double referencePrice = referencePriceSource.get(inMessage.getSecurityID());
                // Might take a long time
                String output = Double.toString(quoteCalculationEngine.calculateQuotePrice(inMessage.getSecurityID(), referencePrice, inMessage.getBuyOrSell(), inMessage.getQuantity()));
                output += "\n";
                byte[] outMessage = new String(output).getBytes();
                buffer.clear();
                buffer.put(outMessage);
                buffer.flip();
                clientSocket.write(buffer);
                
                System.out.println("Response to " + inMessage.getMessage() + " : " + output);
                
                
            }
        } catch(SocketException sE)  {
            try{
            clientSocket.close();
            } catch(IOException ie){
                ie.printStackTrace();
            }
        } catch(Exception e){
            e.printStackTrace();
        }

    }

}
