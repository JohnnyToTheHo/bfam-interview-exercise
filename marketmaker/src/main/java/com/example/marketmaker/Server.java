package com.example.marketmaker;

import java.nio.ByteBuffer;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.io.IOException;

public class Server{
    
    final int BUFFER_SIZE = 100;
    RequestProcessorFactory requestProcessorFactory = new RequestProcessorFactory();
    String hostname = "";
    int tcpPort = 0;
    QuoteCalculationEngine quoteCalculationEngine  = null;
    ReferencePriceSource referencePriceSource = null;
    
    public Server(String hostname, int tcpPort,QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource){
        this.tcpPort = tcpPort;
        this.hostname = hostname;
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
    }

    public void start() throws IOException {
        Selector selector = Selector.open();

        ServerSocketChannel quoteResponseSocket = ServerSocketChannel.open();
        
        InetSocketAddress quoteResponseAddr = new InetSocketAddress(this.hostname, this.tcpPort);

        quoteResponseSocket.bind(quoteResponseAddr);
        System.out.printf("Server started at port %d...\n", tcpPort);
        quoteResponseSocket.configureBlocking(false);
        int ops = quoteResponseSocket.validOps();
        SelectionKey selectionKy = quoteResponseSocket.register(selector, ops, null);
        
        while (true){
            selector.select(); 

            Set<SelectionKey> quoteResponseKeys = selector.selectedKeys();
            Iterator<SelectionKey> quoteResponseIterator = quoteResponseKeys.iterator();

            while (quoteResponseIterator.hasNext()) {
                SelectionKey currentKey = quoteResponseIterator.next();

                // to accept a new socket connection for this key channel
                if (currentKey.isValid() && currentKey.isAcceptable()) {
                    SocketChannel client = quoteResponseSocket.accept();
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ);
                    System.out.println("Connection Accepted: " + client.getLocalAddress() + "\n");

                }else if (currentKey.isValid() &&currentKey.isReadable()){
                    SocketChannel client = (SocketChannel)currentKey.channel(); 
                    ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                    client.read(buffer);

                    String inputString = new String(buffer.array()).trim();
                    if( inputString.length() > 0)   
                        System.out.println("Request received: " + inputString);
                    Message inMessage = new Message();
                    inMessage.setMessage(inputString);
                    
                    RequestProcessor requestProcessor = requestProcessorFactory.createRequestProcessor(inMessage,buffer,client,referencePriceSource,quoteCalculationEngine);
                    Thread t = new Thread(requestProcessor);
                    t.start();
                    
                }
                quoteResponseIterator.remove();
            }
        }
    }

  
}

        

