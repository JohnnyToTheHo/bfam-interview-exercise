package com.example.marketmaker;

import java.io.IOException;
import com.example.marketmaker.objects.QuoteCalculationEngineObject;
import com.example.marketmaker.objects.ReferencePriceSourceObject;
// import com.example.marketmaker.Server;
public class Main {
    
    public static void main(String[] args) throws IOException {
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        // int portNumber = 1212;
        Server server = new Server(hostName, portNumber,new QuoteCalculationEngineObject(),new ReferencePriceSourceObject());
        server.start();

    }
}
