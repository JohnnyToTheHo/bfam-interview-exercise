# Interview Exercise

Implement a server that responds to quote requests.
To run the server: 

1. Navigate to /marketmaker/target
2. use command

`$ java -cp server.jar com.example.marketmaker.Main <hostname> <portNumber>`

To run an example client:

1. Navigate to /client/target
2. Run 

`$ java -cp client.jar com.example.client.Main <hostname> <portNumber>`

## Requirements

The server should accept TCP connections from one or more clients and respond to their requests.  Requests are sent on a 
single line, in the format of:

    {security ID} (BUY|SELL) {quantity}

Where `security ID` and `quantity` are integers.

For example:

    123 BUY 100

Is a request to buy 100 shares of security 123.

This should be responded to with a single line with a single numeric value representing the quoted price.

To calculate the quote price, two interfaces have been provided.

* `QuoteCalculationEngine` - to calculate the quote price based on a security, buy/sell indicator, requested
quantity and reference price.
* `ReferencePriceSource` - source of reference prices for the `QuoteCalculationEngine`.

The server should be capable of handling a large number of quote requests and be able to respond in a timely manner.

## Assumptions

1.	The server will need to scale, thus blocking I/O based on threads will not work. Non-blocking I/O (NIO) is used instead.

NIO server is implemented but any TCP client (IO or NIO) can send requests/ receive responses to the server.

2.	The actual implementation of interface will be provided.

Dummy implementations of the interfaces are included to test if the server runs corretly.

3.	The QuoteCalculationEngine interface might take a long time to run.

After the server receives the quote requests from the clients, the input message is passed to a thread that runs the quoteCalculationEngine interface (the RequestProcessor class). This object will also handle the outbound communication to the client, where the response will be sent after calculations is finished.

Each RequestProcessor thread is created using the RequestProcessorFactory factory class during runtime, in order to decouple the request processing from the server implementation. This allows us to extend the RequestProcessor class without changing the Server implementation.

4.	Requests are sent on a single line of String format. The message protocol is fixed with a format of {integer} {BUY| SELL} {integer}

A message protocol has been defined using the Message.java class. It will handle invalid messages on the client side, therefore any incorrect messages will be parsed before being sent. The error handling is very basic but other error handling methods can be extended using the protocol (Message class), which will be shared among the client and server. 

Due to time constraints, the Message class was copied across client and server packages. In the future it will be put into a jar file and included into the class path.


5.	Assume that client implementation is unknown. However a message protocol must be inplace to ensure correct communication.

The communication protocol is as follows:

    1. Client will send and receive message through TCP.
    2. Client will send messages using the protocol (Message class�s method setMessage and getMessage())
    3. Server will send and receive message through TCP too.

## Testing

The server is written with Test-Driven Development.

The tests in client\test \java\com\example\AppTest.java is written before starting the development and the server is developed based on these tests. There are 4 unit tests that show that the server has these functions:

1. Receiving and responding correctly for a valid request from one client
2. Receiving and responding correctly for a valid request from multiple clients
3. Receiving and responding correctly for invalid request from one client
4. Receiving and responding correctly for invalid request from multiple clients

However the server has to be started manually before the tests are run at port 4444. In the future the server will be started in the Test file using @Before annotations.
