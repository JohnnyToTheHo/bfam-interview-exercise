package com.example;

import static org.junit.Assert.assertEquals;
import com.example.client.IOClient;


import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    
    String hostName = "localhost";
    int portNumber = 4444;
    IOClient client1;
    IOClient client2;


    @Test
    public void Sending_valid_request(){
        client1 = new IOClient();
        
        client1.startConnection(hostName,portNumber);
        
        String resp1 = client1.sendMessage("1 BUY 100");
        assertEquals("100.0",resp1);

        client1.stopConnection();
        
    }

    @Test
    public void Multiple_clients_sending_valid_requests_simultaneously(){
        client1 = new IOClient();
        client2 = new IOClient();
        client1.startConnection(hostName,portNumber);
        client2.startConnection(hostName,portNumber);

        String client1Resp1 = client1.sendMessage("1 BUY 100");
        String client2Resp1 = client2.sendMessage("2 BUY 100");
        assertEquals("100.0",client1Resp1);
        assertEquals("200.0",client2Resp1);

        client1.stopConnection();
        client2.stopConnection();
    }
    
    @Test 
    public void Sending_invalid_request_with_one_client() {
        client1 = new IOClient();
        client1.startConnection(hostName,portNumber);
        
        String resp1 = client1.sendMessage("test");
        String resp2 = client1.sendMessage("1 BUY 100");
        assertEquals("Invalid request",resp1);
        assertEquals("100.0",resp2);

        client1.stopConnection();
        
    }

    @Test 
    public void Sending_valid_and_invalid_requests_simultaneuosly(){
        client1 = new IOClient();
        client2 = new IOClient();
        client1.startConnection(hostName,portNumber);
        client2.startConnection(hostName,portNumber);

        String resp1 = client1.sendMessage("test");
        String resp2 = client2.sendMessage("2 BUY 100");
        assertEquals("Invalid request",resp1);
        assertEquals("200.0",resp2);

        client1.stopConnection();
        client2.stopConnection();
    }
}
