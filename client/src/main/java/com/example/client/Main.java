package com.example.client;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        IOClient client = new IOClient();
        client.startConnection(hostName,portNumber);
        BufferedReader stdIn =new BufferedReader(new InputStreamReader(System.in));
        

    
        String userInput;
        String resp;
        while ((userInput = stdIn.readLine()) != null) {
            
            resp = client.sendMessage(userInput);
            System.out.println("Response: " + resp);
            if(userInput.equals("exit"))
                System.exit(1);
            }
            
        }
        
    
}
