package com.example.client;

import java.io.*;
import java.net.*;

public class IOClient {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String hostName, int portNumber) {
    try{       
        clientSocket = new Socket(hostName, portNumber);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    } catch (UnknownHostException e) {
        System.err.println("Don't know about host " + hostName);
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to " +
            hostName);
    } 
}
    

    public String sendMessage(String msg) {
    try{       
        Message tempMessage = new Message();
        tempMessage.setMessage(msg);
        if(tempMessage.isExitMessage()){
            stopConnection();
            return "Connection closed due to client termination.";
        }
        out.println(tempMessage.getMessage());
        System.out.println("Sending: " + tempMessage.getMessage());
        String resp = in.readLine();
        return resp;
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to " +
            clientSocket.getRemoteSocketAddress());
        stopConnection();
        return "No I/O for connection";
    } 
    }

    public void stopConnection(){
    try{    
        Message tempMessage = new Message();
        tempMessage.setExitMessage();
        out.println(tempMessage.getMessage());
        in.close();
        out.close();
        clientSocket.close();
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to " +
            clientSocket.getRemoteSocketAddress());
    } 
    }
    
}