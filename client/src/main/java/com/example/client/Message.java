package com.example.client;

import java.util.Arrays;
import java.util.List;

public class Message{
    
    final List<String> buyOrSell  = Arrays.asList("BUY","SELL");
    final String exitMessage = "exit";

    private String message = "";
    private boolean isValid = false;
    private String [] buffer;

    public boolean setMessage(String msg){
        if(msg.equals(exitMessage)){
            setExitMessage();
            return true;
        }
        // split into 3 parts
        String[] messageParts = msg.split(" ");
        // check length
        if((messageParts).length!=3){
            setInvalidMessage();
            return false;
        }
        
        // check if first and last is int 
        try {
            int d = Integer.parseInt(messageParts[0]);
            d = Integer.parseInt(messageParts[2]);
        } catch (NumberFormatException nfe) {
            setInvalidMessage();
            return false;
        }
        // check middle is BUY/SELL
        if (!buyOrSell.contains(messageParts[1])){
            setInvalidMessage();
            return false;
        }
        // set this.message = msg
        this.buffer = messageParts;
        this.message = msg;
        this.isValid = true;
        return true;
    } 

    private void setInvalidMessage(){
        this.message =  "Invalid message";
        this.isValid = false;
    }

    public String getMessage(){
        return message;
    }

    public int getSecurityID(){
        if(isValid)
            return Integer.parseInt(buffer[0]);
        else
            return 0;
    }

    public int getQuantity(){
        if(isValid)
            return Integer.parseInt(buffer[2]);
        else
            return 0;
    }

    public boolean getBuyOrSell(){
        if(isValid)
            return buffer[1].equals("BUY");
        else
            return false;
    }

    public boolean isValid(){
        return isValid;
    }

    public void setExitMessage(){
        this.message = exitMessage;
        this.isValid = true;
    }

    public boolean isExitMessage(){
        return this.message.equals(exitMessage) ? true : false;
    }
}
